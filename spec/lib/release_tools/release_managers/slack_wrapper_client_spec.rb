# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::ReleaseManagers::SlackWrapperClient do
  let(:slack_wrapper_url)   { 'https://example.com' }
  let(:slack_wrapper_token) { 'my-secret-token' }

  subject(:client) do
    ClimateControl.modify(SLACK_WRAPPER_URL: slack_wrapper_url, SLACK_WRAPPER_TOKEN: slack_wrapper_token) do
      described_class.new
    end
  end

  describe 'initialize' do
    context 'when SLACK_WRAPPER_URL is not set' do
      let(:slack_wrapper_url) { nil }

      it 'raises an error' do
        expect { client }.to raise_error('Missing environment variable `SLACK_WRAPPER_URL`')
      end
    end

    context 'when SLACK_WRAPPER_TOKEN is not set' do
      let(:slack_wrapper_token) { nil }

      it 'raises an error' do
        expect { client }.to raise_error('Missing environment variable `SLACK_WRAPPER_TOKEN`')
      end
    end

    it { expect { client }.not_to raise_error }
  end

  describe '#sync_membership' do
    it 'sends a POST request with IDS and the provided token' do
      user_ids = %w[UID1 UID2 UID3]
      request = stub_request(:post, "#{slack_wrapper_url}/usergroups.users.update/#{ReleaseTools::Slack::RELEASE_MANAGERS}/UID1,UID2,UID3")
                  .with(headers: { Authorization: "Bearer #{slack_wrapper_token}" })
                  .to_return(body: { ok: true }.to_json)

      client.sync_membership(user_ids)

      expect(request).to have_been_requested
    end

    context 'when the request is unauthorized' do
      it 'keeps track of the error' do
        request =
          stub_request(:post, "#{slack_wrapper_url}/usergroups.users.update/#{ReleaseTools::Slack::RELEASE_MANAGERS}/UID1")
            .to_return(status: 401)

        client.sync_membership(%w[UID1])

        expect(request).to have_been_requested
        expect(subject.sync_errors).to contain_exactly(an_instance_of(ReleaseTools::ReleaseManagers::Client::UnauthorizedError))
      end
    end

    context 'when the request is forbidden' do
      it 'keeps track of the error' do
        request =
          stub_request(:post, "#{slack_wrapper_url}/usergroups.users.update/#{ReleaseTools::Slack::RELEASE_MANAGERS}/UID1")
            .to_return(status: 403)

        client.sync_membership(%w[UID1])

        expect(request).to have_been_requested
        expect(subject.sync_errors).to contain_exactly(an_instance_of(ReleaseTools::ReleaseManagers::Client::UnauthorizedError))
      end
    end

    context "with a Bad request (or any other failure)" do
      it 'keeps track of the error' do
        request =
          stub_request(:post, "#{slack_wrapper_url}/usergroups.users.update/#{ReleaseTools::Slack::RELEASE_MANAGERS}/UID1")
            .to_return(status: 400)

        client.sync_membership(%w[UID1])

        expect(request).to have_been_requested
        expect(subject.sync_errors).to contain_exactly(an_instance_of(ReleaseTools::ReleaseManagers::Client::SyncError))
      end
    end

    context 'when slack returns a failure' do
      it 'keeps track of the error' do
        user_ids = %w[NOTEXISTING]
        request = stub_request(:post, "#{slack_wrapper_url}/usergroups.users.update/#{ReleaseTools::Slack::RELEASE_MANAGERS}/NOTEXISTING")
                    .with(headers: { Authorization: "Bearer #{slack_wrapper_token}" })
                    .to_return(status: 200, body: { ok: false, error: 'invalid_users' }.to_json)

        client.sync_membership(user_ids)

        expect(request).to have_been_requested
        expect(subject.sync_errors).to contain_exactly(an_instance_of(ReleaseTools::ReleaseManagers::Client::SyncError))
        expect(subject.sync_errors.first.message).to eq('invalid_users')
      end
    end
  end

  describe '#members' do
    let(:url) { "#{slack_wrapper_url}/usergroups.users.list/#{ReleaseTools::Slack::RELEASE_MANAGERS}" }

    it 'sends a GET request with the provided token' do
      user_ids = %w[UID1 UID2 UID3]
      request = stub_request(:get, url)
                  .with(headers: { Authorization: "Bearer #{slack_wrapper_token}" })
                  .to_return(body: { ok: true, users: user_ids }.to_json)

      expect(client.members).to eq(user_ids)
      expect(request).to have_been_requested
    end

    context 'when the request is unauthorized' do
      it 'keeps track of the error' do
        request = stub_request(:get, url)
                    .to_return(status: 401)

        expect { client.members }.to raise_error(ReleaseTools::ReleaseManagers::Client::UnauthorizedError)

        expect(request).to have_been_requested
      end
    end

    context 'when the request is forbidden' do
      it 'keeps track of the error' do
        request = stub_request(:get, url)
            .to_return(status: 403)

        expect { client.members }.to raise_error(ReleaseTools::ReleaseManagers::Client::UnauthorizedError, 'Invalid token')

        expect(request).to have_been_requested
      end
    end

    context "with a Bad request (or any other failure)" do
      it 'keeps track of the error' do
        request = stub_request(:get, url)
            .to_return(status: 400)

        expect { client.members }.to raise_error(ReleaseTools::ReleaseManagers::Client::SyncError, 'Bad Request')

        expect(request).to have_been_requested
      end
    end
  end

  describe '#join' do
    let(:user_ids) { %w[UID1 UID2 UID3] }
    let!(:members_request) do
      stub_request(:get, "#{slack_wrapper_url}/usergroups.users.list/#{ReleaseTools::Slack::RELEASE_MANAGERS}")
        .to_return(body: { ok: true, users: user_ids }.to_json)
    end

    it 'adds a new user to the group' do
      new_user = 'UID4'

      expect(subject).to receive(:sync_membership).with(user_ids + [new_user])

      client.join(new_user)

      expect(members_request).to have_been_requested
    end

    it 'does not add users already belonging to the group' do
      new_user = user_ids[0]

      expect(subject).not_to receive(:sync_membership)

      client.join(new_user)

      expect(members_request).to have_been_requested
    end
  end

  describe '#leave' do
    let(:user_ids) { %w[UID1 UID2 UID3] }
    let!(:members_request) do
      stub_request(:get, "#{slack_wrapper_url}/usergroups.users.list/#{ReleaseTools::Slack::RELEASE_MANAGERS}")
        .to_return(body: { ok: true, users: user_ids }.to_json)
    end

    it 'removes a user from the group' do
      new_user = user_ids[0]

      expect(subject).to receive(:sync_membership).with(user_ids - [new_user])

      client.leave(new_user)

      expect(members_request).to have_been_requested
    end

    it 'does not remove users not belonging to the group' do
      new_user = 'UID4'

      expect(subject).not_to receive(:sync_membership)

      client.leave(new_user)

      expect(members_request).to have_been_requested
    end
  end
end
