# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Project::ReleaseTools do
  it_behaves_like 'project .remotes'
  it_behaves_like 'project .to_s'

  describe '.id' do
    it 'returns the canonical id' do
      expect(described_class.id).to eq(described_class::IDS.fetch(:canonical))
    end

    context 'without a remote defined' do
      it 'raises an error' do
        stub_const("ReleaseTools::Project::ReleaseTools::IDS", { foo: 1, bar: 2 })

        expect do
          described_class.id
        end.to raise_error('Invalid remote for gitlab-org/release-tools: canonical')
      end
    end
  end
end
