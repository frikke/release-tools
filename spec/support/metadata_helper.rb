# frozen_string_literal: true

# Helpers for building release metadata
module MetadataHelper
  def build_metadata(
    auto_deploy_branch: '42.1.2021110116',
    gitlab_sha: 'ad89f6752f3',
    omnibus_sha: 'ef94506739c',
    tag: true
  )

    {
      'releases' => {
        'gitlab-ee' => {
          'version' => gitlab_sha,
          'sha' => gitlab_sha,
          'ref' => auto_deploy_branch,
          'tag' => tag
        },
        'omnibus-gitlab-ee' => {
          'version' => omnibus_sha,
          'sha' => omnibus_sha,
          'ref' => "#{auto_deploy_branch}+#{gitlab_sha[0, 11]}.#{omnibus_sha[0, 11]}",
          'tag' => tag
        }
      }
    }
  end
end
