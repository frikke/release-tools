# frozen_string_literal: true

require 'http'

module ReleaseTools
  module ReleaseManagers
    # A client for https://ops.gitlab.net/gitlab-com/gl-infra/infra-automation-commons/slack-wrapper
    class SlackWrapperClient
      include ::SemanticLogger::Loggable

      attr_reader :sync_errors, :token, :endpoint

      # Initialize a slack-wrapper API client specific to Release Manager tasks
      def initialize
        @endpoint = ENV.fetch('SLACK_WRAPPER_URL') do |name|
          raise "Missing environment variable `#{name}`"
        end

        @token = ENV.fetch('SLACK_WRAPPER_TOKEN') do |name|
          raise "Missing environment variable `#{name}`"
        end

        @sync_errors = []
      end

      def sync_membership(user_ids)
        url = File.join(user_group_update_url, user_ids.join(','))
        logger.info('Syncing membership', user_ids: user_ids, url: url)

        response = client.post(url)
        parse_response(response)
      rescue Client::SyncError => ex
        sync_errors << ex
      end

      def members
        url = user_group_list_url

        logger.info('Fetching membership', url: url)
        response = client.get(url)
        resp = parse_response(response)

        resp['users']
      end

      def join(user_id)
        current_members = members
        if current_members.include?(user_id)
          logger.warn('User is already part of the group', user_id: user_id, members: current_members)
          return
        end

        sync_membership(current_members + [user_id])
      end

      def leave(user_id)
        current_members = members
        unless current_members.include?(user_id)
          logger.warn('User is not part of the group', user_id: user_id, members: current_members)
          return
        end

        sync_membership(current_members - [user_id])
      end

      private

      def user_group_update_url
        File.join(endpoint, 'usergroups.users.update', ReleaseTools::Slack::RELEASE_MANAGERS)
      end

      def user_group_list_url
        File.join(endpoint, 'usergroups.users.list', ReleaseTools::Slack::RELEASE_MANAGERS)
      end

      def client
        HTTP.auth("Bearer #{@token}")
      end

      def parse_response(response)
        unless response.status.success?
          case response.status.to_sym
          when :unauthorized
            raise Client::UnauthorizedError, 'Unauthorized'
          when :forbidden
            raise Client::UnauthorizedError, 'Invalid token'
          else
            raise Client::SyncError, response.status.reason
          end
        end

        resp = JSON.parse(response.body)
        unless resp['ok']
          logger.error('Slack release-managers group update failed', response_body: resp)
          raise Client::SyncError, resp['error'] || 'unknown error'
        end

        resp
      end
    end
  end
end
