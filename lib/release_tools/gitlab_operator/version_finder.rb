# frozen_string_literal: true

module ReleaseTools
  module GitlabOperator
    class VersionFinder
      # The regex to use for extracting GitLab Chart versions from tag messages.
      TAG_REGEX = /supports GitLab Charts (?<versions>[\d .,]*)/.freeze

      def initialize(client = GitlabClient)
        @client = client
      end

      def latest_version(versions = nil)
        versions ||= release_tags.keys

        max = sorted_versions(versions).last
        Version.new(max)
      end

      def release_tags
        @release_tags ||= {}.tap do |tags|
          @client
            .tags(Project::GitlabOperator)
            .auto_paginate do |tag|
              matches = tag.message.match(TAG_REGEX)

              next unless matches

              versions = matches[:versions].split(", ")
              tags[tag.name] = versions
            end
        end
      end

      def execute(chart_version)
        existing_versions = release_tags.map { |tag, versions| Version.new(tag) if versions.include?(chart_version) }.compact

        return sorted_versions(existing_versions).last unless existing_versions&.empty?

        # For the time being, we are not considering backports for GitLab
        # Operator. So, this method can be simple and just return the next
        # patch/minor version after the latest version. However, when we start
        # doing backports, this will need a refactor, and we will have to
        # identify the version information parsing the tag messages, similar to
        # how we do in HelmVersionFinder
        if chart_version.patch?
          Version.new(latest_version.next_patch)
          # For now, we are still on 0.1.x releases, hence returning minor versions only.
        else
          Version.new(latest_version.next_minor)
        end
      end

      def sorted_versions(versions)
        ReleaseTools::Versions.sort(versions)
      end
    end
  end
end
