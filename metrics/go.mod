module gitlab.com/gitlab-org/release-tools/metrics

go 1.15

require (
	github.com/gorilla/mux v1.8.0
	github.com/prometheus/client_golang v1.12.2
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.7.2
	github.com/xanzy/go-gitlab v0.68.0
	gitlab.com/gitlab-org/labkit v1.15.0
)
